
public class LinkList {
	Node head;
	/**
	 * Node 
	 * @author piyushnishant
	 *
	 */
	class Node{
		int data;
		Node link;
		public Node(int info){
			this.data = info;
			link = null;
		}
	}
	/**
	 * Insert Node at end
	 * @param data
	 */
	void insertNode(int data) {
		if(head == null) {
			Node temp = new Node(data);
			head  = temp;
			return;
		}
		Node q;
		q = head; 
		while(q.link != null) {
			q = q.link;
		}
		Node temp = new Node(data);
		q.link = temp;
		q = temp;
	}
	/**
	 * Display All Nodes
	 */
	void displayList() {
		if(head == null) {
			System.out.println("There is no element");
			return;
		}
		Node q = head;
		while(q != null) {
			System.out.print(""+q.data+" ");
			q = q.link;
		}
		System.out.println("");
		
	}
	/**
	 * Print Middle of data
	 */
	void printMiddleNode() {
		Node slow;
		Node fast;
		Node prev;
		slow = head;
		fast = head;
		prev = head;
		while(fast != null && fast.link != null) {
			fast = fast.link.link;
			prev = slow;
			slow = slow.link;
		}
		System.out.println(""+slow.data);
		System.out.println(""+prev.data);
	}
	
}
